import {p, sendSignal, sendOrDiscard, connected} from "./connectionHandler.js";
import * as d  from "dom99/source/dom99.js";
import {move} from "dom99/plugins/move/move.js";
import {setupBabylon, move as movex} from "./setupBabylon.js";
import {playSound} from "./sound.js";

setTimeout(() => {
    playSound("music");
}, 10000)
let speed = 0.1;

d.functions.moveLeft = function (event) {
    movex(0,-speed)
};
d.functions.moveUp = function (event) {
    movex(speed,0)
};
d.functions.moveRight = function (event) {
    movex(0, speed)
};
d.functions.moveDown = function (event) {
    movex(-speed, 0)
};
d.functions.sendSignal = (event) => {
    event.preventDefault();
    sendSignal(d.get(`otherSignal`));
};

d.functions.clickOwnSignal = () => {
    d.element(`pre`).focus();
    d.element(`pre`).select();
    document.execCommand('copy');
};

d.plugin(move);
d.start();
d.feed({
    otherSignal: ``,
    mySignal: ``,
});
setupBabylon();

  
p.on('connect', function () {
    d.element(`form`).remove();
    d.element(`pre`).remove();
    sendOrDiscard('CONNECT');
})

const speed2 = 0.1;
p.on('data', function (data) {
    console.log(String(data));
    const [front, side] = String(data).split("_");
    let x = 0;
    let z = 0;
    if (front === "up") {
        x += speed2;
    } else {
        x += -speed2
    }
    if (side === "Left") {
        z += speed2;
    } else {
        z += -speed2;
    }
    movex(x, z);
});
window.movex = movex;

p.on('signal', function (data) {
    console.log('own SIGNAL is ready')
    d.feed({mySignal: JSON.stringify(data)});
    d.functions.clickOwnSignal();
});

window.p = p;
