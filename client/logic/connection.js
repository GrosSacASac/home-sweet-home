import {p, sendSignal, sendOrDiscard, connected} from "./connectionHandler";

document.querySelector('form').addEventListener('submit', function (ev) {
  ev.preventDefault();
  sendSignal(document.querySelector('#incoming').value);
})

p.on('connect', function () {
  console.log('CONNECT')
  sendOrDiscard('whatever' + Math.random());
  sendOrDiscard({hi: 'hi'});
})


p.on('data', function (data) {
  console.log('data: ')
  console.log(String(data));
})


p.on('signal', function (data) {
  console.log('SIGNAL', JSON.stringify(data))
  document.querySelector('#outgoing').textContent = JSON.stringify(data)
});
