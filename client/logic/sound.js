export {playSound};

const music = document.getElementById("music");


const sounds = {
    music,
};

const playSound = (name) => {
    if (!sounds[name]) {
        console.warning(`Sound ${name} does not exist`);
        return;
    }
    sounds[name].play();
};
