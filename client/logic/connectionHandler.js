// import 'simple-peer/simplepeer.min.js';
const Peer = window.SimplePeer;

export {p, sendSignal, sendOrDiscard, connected};

let connected = false;
const sendOrDiscard = (x) => {
  if (!connected) {
    return;
  }
  p.send(x);
}

const p = new Peer({ initiator: String(location.href).includes("controller"), trickle: false })

p.on('error', function (err) { console.log('error', err) })


const sendSignal = function (signalString) {
  p.signal(JSON.parse(signalString));
};

p.on('connect', function () {
  connected = true;
});

Peer.config.iceServers.unshift({url: "stun:stun.voipbuster.com"});
Peer.config.iceServers.unshift({url: "stun:stun.l.google.com:19302"});


// p.on('data', function (data) {
//   console.log('data: ' + data)
// })


// p.on('signal', function (data) {
//   console.log('SIGNAL', JSON.stringify(data))
//   document.querySelector('#outgoing').textContent = JSON.stringify(data)
// })