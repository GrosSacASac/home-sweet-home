import CANNON from "cannon/build/cannon.min.js";
window.CANNON = CANNON;
import * as BABYLON from '@babylonjs/core/index.js'
import * as Loaders from '@babylonjs/loaders/index.js'
import * as d  from "dom99/source/dom99.js";
import {Collisions, Circle, Polygon, Point} from 'collisions';
import {walls} from "./walls";
export {setupBabylon, move};

const system = new Collisions();
// Create a Result object for collecting information about the collisions
const result = system.createResult();
const player = new Circle(10, 8, 0.6);
const offset = 50;
window.player = player;
let camera;
const fixedY = 1.7;
const initialPosition = [5, 5];
const canvas = document.getElementById("renderCanvas");
const engine = new BABYLON.Engine(canvas, true);

const setupBabylon = () => {

    window.addEventListener("resize", function () {
        engine.resize();
    });
    
    
    BABYLON.SceneLoader.Load("./3d/", "home.gltf", engine, function (scene) {
        let debugCollision;
        try {
            setupScene(scene)
            camera = setupCamera(scene);
            setupLight();
            setUpGround();
            debugCollision = setupDebugCollision();
            setPlayerPosition(...initialPosition);
        } catch(error) {
            console.error(error);
        }
        

        engine.runRenderLoop(function() {
            camera.position.y = fixedY;
            copyPositionFromCamera(camera, player);
            // setPlayerPosition(player.x, player.y);
            system.update();
            const potentials = player.potentials();

            // Loop through the potential wall collisions
            for(const wall of potentials) {
                // Test if the player collides with the wall
                if(player.collides(wall, result)) {
                    // Push the player out of the wall
                    setPlayerPosition(
                        player.x - result.overlap * result.overlap_x,
                        player.y - result.overlap * result.overlap_y
                        );
                    console.log('colliding')
                }
            }
            scene.render();
            debugCollision();

        });
    });
};


const setupScene = (scene) => {
    scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
    scene.collisionsEnabled = true;
    scene.enablePhysics();
    setUpTexture(scene);
    system.insert(player);
    createWalls();
    return scene;
 };
  
const setupCamera = (scene) => {
    camera = new BABYLON.UniversalCamera("Camera", new BABYLON.Vector3(1, 1, 0), scene);
    //inclinaison de la camera
    camera.noRotationConstraint=true;
    camera.attachControl(canvas, true);
    //Controle à la souris (plus besoin de drag & drop)
    window.addEventListener("mousemove", function (event) {
        camera.cameraRotation.x += event.movementY > 0 ? -0.01 : event.movementY < 0 ? 0.01 : 0;
        camera.cameraRotation.y += event.movementX > 0 ? 0.01 : event.movementX < 0 ? -0.01 : 0;
    })
    
    return camera;
 };

 const setupLight = (scene) => {
    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    const light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;
 };

 const setUpGround = (scene) => {
    const ground = BABYLON.Mesh.CreateGround("ground1", 60, 60, 2, scene);
    ground.material = new BABYLON.StandardMaterial("ground", scene);
    ground.material.diffuseColor = new BABYLON.Color3(.2, .1, .05);
    ground.position.y = -0.1;
 };

 const setUpTexture = (scene) => {
    const hdrTexture = BABYLON.CubeTexture.CreateFromPrefilteredData("./3d/environment.dds", scene);
    hdrTexture.name = "envTex";
    hdrTexture.gammaSpace = false;
    scene.createDefaultSkybox(hdrTexture, true, 1000, 0);
 };

 const myCollide = function() {
    //sphere.material.diffuseColor = new BABYLON.Color3.Random();
    console.log("boing");
}

const createWalls = () => {
    const walloffset = offset - 20;
    walls.forEach(({width, depth, x, z}) => {
        x = -x + walloffset;
        z = -z + walloffset;
        const angle = 0;
        const wall = system.createPolygon(x, z, [[x, z], [x + width, z], [x + width, z + depth], [x, z + depth]], 0.2);
    });    
};

const setupDebugCollision = () => {
    const canvas = document.getElementById("debugCanvas");
    const context = canvas.getContext("2d");
    const debugCollision = () => {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.strokeStyle = '#000000';
        context.beginPath();
        system.draw(context);
        context.stroke();
    };
    return debugCollision;
};

const move = (x, y) => {
    setPlayerPosition(x + player.x - offset, y + player.y - offset);
};

const setPlayerPosition = (x, z) => {
    camera.position.x = x;
    camera.position.z = z;
    player.x = x + offset;
    player.y = z + offset;
};

const copyPositionFromCamera = (camera, player) => {

    player.x = camera.position.x + offset;
    player.y = camera.position.z + offset;
}