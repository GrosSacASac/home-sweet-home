export {walls};

const walls = [
    {width: .2, depth: 2.5,
        x: .1, y:1.15, z:-1.8}
        
        ,{ width: 3.4, depth: .2,
        x: 1.9, y:1.15, z:-3}
        
        ,{ width: .2, depth: 4,
        x: 3.55, y:1.15, z:-1}
        
        ,{ width: 3.2, depth: .2,
        x: 5.2, y:1.15, z:0.6}
        
        ,{ width: .2, depth: 3,
        x: 6.7, y:1.15, z:2.2}
        
        ,{ width: 5, depth: .4,
        x: 5.6, y:1.15, z:3.9}
        
        ,{ width: 3.8, depth: .4,
        x: .3, y:1.15, z:3.9}
        
        ,{ width: 23, depth: .4,
        x: -11.5, y:1.15, z:3.9}
        
        ,{ width: 30, depth: .4,
        x: -7, y:1.15, z:6.3}
        
        ,{ width: .4, depth: 2.4,
        x: 8.2, y:1.15, z:5}
        
        ,{ width: .4, depth: 2.4,
        x: -21.5, y:1.15, z:5}
        
        ,{ width: 7.0, depth: 1.1,
        x: -5.05, y:1.15, z:3.25}
        
        ,{ width: .2, depth: 5.4,
        x: -8.6, y:1.15, z:0.5}
        
        ,{ width: 8.6, depth: .2,
        x: -4.3, y:1.15, z:-2.1}
        
        ,{ width: 2.5, depth: .2,
        x: 1.0, y:1.15, z:-.4}
        
        ,{ width: 3.0, depth: .2,
        x: -2.8, y:1.15, z:-.4}
        
        ,{ width: .2, depth: 3.4,
        x: -4.4, y:1.15, z:1.0}
        
        ,{ width: .2, depth: 1.9,
        x: -1.5, y:1.15, z:1.7}
];



