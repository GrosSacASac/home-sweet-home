import {p, sendSignal, sendOrDiscard, connected} from "./connectionHandler.js";
// https://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
const support = typeof DeviceOrientationEvent !== "undefined";
const span = document.getElementById("span");

const createThrottledFunction = function (functionToThrottle, minimumTimeSpace) {
    /* creates a function that is throttled,
    calling it once will execute it immediately
    calling it very often during a period less than minimumTimeSpace will only execute it once
    the returned function always return undefined

    an alternative implementation could use Date.now() , this means less performance
    but would work for throttling inside a single long tick
    */
    let ready = true;
    const makeReady = function() {
        ready = true;
    };
    return function(...args) {
        if (!ready) {
            return;
        }
        ready = false;
        functionToThrottle(...args);
        setTimeout(makeReady, minimumTimeSpace);
    };
};

const deviceOrientationListener = function (event) {
    var beta = event.beta;
    var gamma =  event.gamma;
    let direction;
    if (gamma > 0) {
        direction = "down";
    } else {
        direction = "up";
    }
    if (beta < 0) {
        direction += "_Right";
    } else {
        direction += "_Left";
    }
    send(direction);
}

const send = (x) => {
    console.log(x);
    span.textContent = x;
    sendOrDiscard(x);
};

window.send = send;

if (support) {
    window.addEventListener("deviceorientation", createThrottledFunction(deviceOrientationListener, 100));
} else {
    console.error("not supported");
}


document.querySelector('form').addEventListener('submit', function (ev) {
  ev.preventDefault();
  sendSignal(document.querySelector('#incoming').value);
})

p.on('connect', function () {
    span.textContent = 'connect';
})


p.on('data', function (data) {
  console.log('data: ')
  console.log(String(data));
})

const outgoing = document.querySelector('#outgoing');
p.on('signal', function (data) {
  console.log('SIGNAL', JSON.stringify(data))
  outgoing.value = JSON.stringify(data)
}); 

outgoing.addEventListener("click", () => {
    outgoing.focus();
    outgoing.select();
    document.execCommand('copy');
});