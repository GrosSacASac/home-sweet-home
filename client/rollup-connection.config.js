import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

// import nodegloabals from 'rollup-plugin-node-globals';
import { uglify } from 'rollup-plugin-uglify';
// import builtins from 'rollup-plugin-node-builtins';

// `npm run build` -> `production` is true
// `npm run dev` -> `production` is false
const production = !process.env.ROLLUP_WATCH;

export default {
	input: 'client/logic/connection.js',
	output: {
		file: 'client/logic/connectionBundle.js',
		format: 'esm', // immediately-invoked function expression — suitable for <script> tags
		sourcemap: true
	},
	plugins: [
		commonjs(), // converts date-fns to ES modules
		// nodegloabals(),
		// builtins(),
		resolve(), // tells Rollup how to find date-fns in node_modules
		production && uglify() // minify, but only in production
	]
};