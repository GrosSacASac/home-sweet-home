

import {Collisions, Circle, Polygon, Point} from './node_modules/collisions/src/Collisions.mjs';



var canvas = document.getElementById("renderCanvas");
const canvas2 = document.getElementById('collisionCanvas');
const context = canvas2.getContext('2d');

var collisionPlayer;
var collisionSphere;
var collisionMur1, collisionMur2, collisionMur3, collisionMur4, collisionMur5, collisionMur6, collisionMur7, collisionMur8;
var collisionMobilier;
var resultMur;
var resultMobilier;
var resultLaine;

var camera;
var sphere;
var boxCollider1, boxCollider2, boxCollider3, boxCollider4, boxCollider5, boxCollider6, boxCollider7, boxCollider8;


let SetupCollisions = function(){
     collisionMobilier = new Collisions();

    collisionSphere  = new Circle(100, 100, 2);
    collisionPlayer  = new Circle(80, 80, 2);
    collisionMur1 = new Polygon(.1, -1.8, [[-.1, -1.25],[-.1, 1.25],[0.1, -1.25],[.1, 1.25]]) 

    collisionMobilier.insert(collisionSphere);
    collisionMobilier.insert(collisionPlayer);
    collisionMobilier.insert(collisionMur1);

    resultMobilier = collisionMobilier.createResult();
}

var uncollideMobilier = function(result){
    console.log(result);

}

var createScene = function() {
    var scene = new BABYLON.Scene(engine);
    scene.collisionsEnabled = true;
    scene.gravity = new BABYLON.Vector3(0, -4, 0);

    // Setup camera
    //camera = new BABYLON.ArcRotateCamera("Camera", Math.PI / 2, 1.1, 20, new BABYLON.Vector3(0, 1, 0), scene); 
    camera = new BABYLON.UniversalCamera("Camera", new BABYLON.Vector3(0, 1, 0), scene);
    camera.attachControl(canvas, true);
    camera.ellipsoid = new BABYLON.Vector3(1, 1, 1);
    camera.checkCollisions = true;
    camera.applyGravity = true;
    //camera.position = BABYLON.Vector3(-10,10,25);

    // Lights
    var light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 0.5;

    scene.clearColor = new BABYLON.Color3(0.1, 0.1, 0.5); // Background color gray (0.5, 0.5, 0.5)

    // Ball
     sphere = BABYLON.Mesh.CreateSphere("sphere", 8, 2, scene);
    sphere.checkCollisions = true;
    sphere.material = new BABYLON.StandardMaterial("textureS", scene);
    sphere.material.diffuseColor = new BABYLON.Color3(0, 1, 1);
    sphere.position.y = 10;
    sphere.position.x = -5;

     // Ground
    var ground = BABYLON.Mesh.CreateGround("ground", 100, 100, 2, scene);
    ground.material = new BABYLON.StandardMaterial("ground", scene);
    ground.material.diffuseColor =  new BABYLON.Color3(.2, .1, .05);

    ground.checkCollisions = true

    //Walls
    var boxCollider1 = BABYLON.MeshBuilder.CreateBox("box1", {height: 2.3, width: .2, depth: 2.5}, scene);
    boxCollider1.position = {x: .1, y:1.15, z:-1.8};
    boxCollider1.checkCollisions = true;
    


    // Physics! 
    scene.enablePhysics();

    sphere.physicsImpostor = new BABYLON.PhysicsImpostor(sphere, BABYLON.PhysicsImpostor.SphereImpostor,
    { 
        mass: 1.0, restitution: 1.0 
    }, scene);

    ground.physicsImpostor = new BABYLON.PhysicsImpostor(ground, BABYLON.PhysicsImpostor.BoxImpostor, 
    { 
        mass: 0, restitution: 1.0 
    }, scene);


// -------------------------------------------------
   
/*var myCollide = function() {
        //console.log("bounce!");
        sphere.material.diffuseColor = new BABYLON.Color3.Random();
    }


    sphere.physicsImpostor.registerOnPhysicsCollide(ground.physicsImpostor, myCollide);
    ground.physicsImpostor.registerOnPhysicsCollide(sphere.physicsImpostor, myCollide);


//    sphere.physicsImpostor.onCollide = myCollide;
  //  ground.physicsImpostor.onCollide = myCollide;


    sphere.onPhysicsCollide = myCollide;
    ground.onPhysicsCollide = myCollide;
*/
    // -------------------------------------------------



    scene.registerBeforeRender(function () {
        if (sphere.position.y < ground.position.y - 20){
            sphere.position = new BABYLON.Vector3(0, 5, 0);
            // kill all positional movement
            sphere.physicsImpostor.setLinearVelocity(new BABYLON.Vector3(0,0,0));       
            // kill all rotational movement
            sphere.physicsImpostor.setAngularVelocity(new BABYLON.Vector3(0,0,0));       
        }
   });

    return scene;
};

SetupCollisions();

var engine = new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true });
var scene = createScene(engine);


engine.runRenderLoop(() => {
    if (scene) {
        //console.log(Object.keys(camera.position))
        collisionPlayer.x = camera.position.x;
        collisionPlayer.y = camera.position.z;

        collisionSphere.y = sphere.position.z;
        collisionSphere.x = sphere.position.x;

        collisionMobilier.update();
        var potentials = collisionPlayer.potentials();
        for(const wall of potentials){
            //console.log(wall);
            //Test if the player collides with the wall
            if(collisionPlayer.collides(wall, resultMobilier)){
                uncollideMobilier(wall);
                console.log(Object.keys(wall))
            }
        }
        context.strokeStyle = '#000000';
        context.beginPath();

        collisionMur1.draw(context);
        collisionSphere.draw(context);
        collisionPlayer.draw(context);
        context.stroke();

        scene.render();
    }
})
