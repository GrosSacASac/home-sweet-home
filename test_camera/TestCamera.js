var canvas = document.getElementById("renderCanvas");

var createScene = (engine) => {

    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);
    
    scene.collisionsEnabled = true;
    scene.gravity = new BABYLON.Vector3(0, -9.81, 0);

    // This creates and positions a free camera (non-mesh)
    var camera = new BABYLON.UniversalCamera("camera1", new BABYLON.Vector3(0, 5, -10), scene);

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);


    camera.noRotationConstraint=true;
    camera.checkCollisions = true;
    camera.ellipsoid = new BABYLON.Vector3(0.5, 1, 0.5);

   


    //camera.applyGravity = true;
    
   
   //inclinaison de la camera
    camera.upVector = new BABYLON.Vector3(0.2,1,0.2);

    camera.rollCorrect = 100;




    // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
    var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Our built-in 'sphere' shape. Params: name, subdivs, size, scene
    var sphere = BABYLON.Mesh.CreateSphere("sphere1", 16, 2, scene);
    sphere.material = new BABYLON.StandardMaterial("textureS", scene);
    sphere.material.diffuseColor = new BABYLON.Color3(0, 1, 1);

    // Move the sphere upward 1/2 its height
    sphere.position.y = 10;

    // Our built-in 'ground' shape. Params: name, width, depth, subdivs, scene
    var ground = BABYLON.Mesh.CreateGround("ground1", 60, 60, 2, scene);
    ground.material = new BABYLON.StandardMaterial("ground", scene);
    ground.material.diffuseColor =  new BABYLON.Color3(.2, .1, .05);




    ground.checkCollisions = true;
    sphere.checkCollisions = true;



// Physics! 
scene.enablePhysics();

sphere.physicsImpostor = new BABYLON.PhysicsImpostor(sphere, BABYLON.PhysicsImpostor.SphereImpostor,
{ 
    mass: 1.0, restitution: 0.5 
}, scene);


ground.physicsImpostor = new BABYLON.PhysicsImpostor(ground, BABYLON.PhysicsImpostor.BoxImpostor, 
    { 
        mass: 0.0, restitution: 1.0 
    }, scene);






var myCollide = function() {
    sphere.material.diffuseColor = new BABYLON.Color3.Random();
    
    console.log("balle en contact");
}

var cameraCollide = function() {
    console.log("camera en contact");
    if(sphere.intersectsMesh(camera.ellipsoid, false)) console.log("camera + sphere!");
}
    
    sphere.physicsImpostor.registerOnPhysicsCollide(sphere.physicsImpostor, myCollide);
  //  ground.physicsImpostor.registerOnPhysicsCollide(ground.physicsImpostor, myCollide);


    sphere.physicsImpostor.onCollide = myCollide;

    camera.onCollide = function (colMesh) {
            console.log("vous touchez " + colMesh.name);
		/*if (colMesh.uniqueId === sphere.uniqueId) {
			console.log("Vous avez touché la caisse! (enfin!)");
		}*/
	}

    



    //Controle à la souris (plus besoin de drag & drop)
    window.addEventListener("mousemove", function (event) {
        camera.cameraRotation.x += event.movementY > 0 ? -0.01 : event.movementY < 0 ? 0.01 : 0;
        camera.cameraRotation.y += event.movementX > 0 ? 0.01 : event.movementX < 0 ? -0.01 : 0;
    })

    scene.registerBeforeRender(function () {
        if (sphere.position.y < ground.position.y - 20){
            sphere.position = new BABYLON.Vector3(0, 5, 0);
            // kill all positional movement
            sphere.physicsImpostor.setLinearVelocity(new BABYLON.Vector3(0,0,0));       
            // kill all rotational movement
            sphere.physicsImpostor.setAngularVelocity(new BABYLON.Vector3(0,0,0));       
        }
   });
    
    return scene;
};

var engine = new BABYLON.Engine(canvas, true, { preserveDrawingBuffer: true, stencil: true });
var scene = createScene(engine);

engine.runRenderLoop(() => {
    if (scene) {
        scene.render();
    }
})


